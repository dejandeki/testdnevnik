package com.example.DnevnikTest.App.service;

import org.springframework.stereotype.Service;

import com.example.DnevnikTest.App.dao.PredmetiDAO;
import com.example.DnevnikTest.App.entity.Predmeti;
@Service("predmetiService")
public class PredmetiServiceImpl extends GenericServiceImpl<Predmeti, PredmetiDAO> implements PredmetiService {

	
}
