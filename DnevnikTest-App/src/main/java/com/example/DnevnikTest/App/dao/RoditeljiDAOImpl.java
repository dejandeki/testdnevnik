package com.example.DnevnikTest.App.dao;

import org.springframework.stereotype.Repository;

import com.example.DnevnikTest.App.entity.Roditelji;

@Repository("roditeljiDAO")
public class RoditeljiDAOImpl extends GenericDAOImpl<Roditelji> implements RoditeljiDAO {

	public RoditeljiDAOImpl() {
		super(Roditelji.class);
		// TODO Auto-generated constructor stub
	}

}
