package com.example.DnevnikTest.App;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.DnevnikTest.App.entity.Korisnici;
import com.example.DnevnikTest.App.service.KorisniciService;

@Controller
public class KorisnikController {

	@Autowired
	private KorisniciService korisniciService;
	
	@RequestMapping("/list2")
	public String listKorisnici(Model theModel) {

		// get korisnici 
		
		List<Korisnici> theKorisnici = korisniciService.getAll();

		// add the korisnici to the model
		theModel.addAttribute("korisnici", theKorisnici);

		return "testfile";
	}
}
