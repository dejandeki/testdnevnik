package com.example.DnevnikTest.App;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.DnevnikTest.App.entity.Predmeti;
import com.example.DnevnikTest.App.service.PredmetiService;

@Controller
public class PredmetiController {

	
	@Autowired
	private PredmetiService predmetiService;
	
	@RequestMapping("/list4")
	public String listPredmeti(Model theModel) {

		// get cats 
		List<Predmeti> thePredmeti = predmetiService.getAll();

		// add the cats to the model
		theModel.addAttribute("predmeti", thePredmeti);

		return "listpredmeti";
	}
}
