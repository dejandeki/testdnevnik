package com.example.DnevnikTest.App.dao;

import org.springframework.stereotype.Repository;

import com.example.DnevnikTest.App.entity.Korisnici;



@Repository("korisniciDAO")
public class KorisniciDAOImpl extends GenericDAOImpl<Korisnici> implements KorisniciDAO {

	public KorisniciDAOImpl() {
		super(Korisnici.class);
	}

}
