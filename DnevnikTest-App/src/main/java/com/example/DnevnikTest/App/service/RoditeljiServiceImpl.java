package com.example.DnevnikTest.App.service;

import org.springframework.stereotype.Service;

import com.example.DnevnikTest.App.dao.RoditeljiDAO;
import com.example.DnevnikTest.App.entity.Roditelji;
@Service("roditeljiService")
public class RoditeljiServiceImpl extends GenericServiceImpl<Roditelji, RoditeljiDAO> implements RoditeljiService {

	
}
