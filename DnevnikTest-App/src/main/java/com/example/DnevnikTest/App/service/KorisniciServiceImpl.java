package com.example.DnevnikTest.App.service;

import org.springframework.stereotype.Service;

import com.example.DnevnikTest.App.dao.KorisniciDAO;
import com.example.DnevnikTest.App.entity.Korisnici;

@Service("korisniciService")
public class KorisniciServiceImpl extends GenericServiceImpl<Korisnici, KorisniciDAO> implements KorisniciService {

	
}
