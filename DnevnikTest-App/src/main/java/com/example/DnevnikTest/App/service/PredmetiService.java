package com.example.DnevnikTest.App.service;

import com.example.DnevnikTest.App.entity.Predmeti;

public interface PredmetiService extends GenericService<Predmeti> {

}
