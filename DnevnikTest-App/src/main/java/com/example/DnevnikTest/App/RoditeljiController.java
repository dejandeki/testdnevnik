package com.example.DnevnikTest.App;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.DnevnikTest.App.entity.Roditelji;
import com.example.DnevnikTest.App.service.RoditeljiService;

@Controller
public class RoditeljiController {

	
	@Autowired
	private RoditeljiService roditeljiService;
	
	@RequestMapping("/list3")
	public String listRoditelji(Model theModel) {

		// get cats 
		List<Roditelji> theRoditelji = roditeljiService.getAll();

		// add the cats to the model
		theModel.addAttribute("roditelji", theRoditelji);

		return "selectroditelji";
	}
}
