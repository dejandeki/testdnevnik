package com.example.DnevnikTest.App.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="korisnici")
public class Korisnici {

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_korisnik")
	private int id_korisnik;
	@Column(name="ime")
	private String ime;
	@Column(name="prezime")
	private String prezime;
	@Column(name="username")
	private String username;
	@Column(name="password")
	private String password;
	@Column(name="adresa")
	private String adresa;
	@Column(name="telefon")
	private String telefon;
	@Column(name="email")
	private String email;
	
	//@ManyToOne
	//@JoinColumn(name="uloge_uloga_id")
	//private Uloge uloga;
	private int uloge_uloga_id;
	public Korisnici() {}

	
	


	public Korisnici(String ime, String prezime, String username, String password, String adresa, String telefon,
			String email, int uloge_uloga_id) {
		super();
		this.ime = ime;
		this.prezime = prezime;
		this.username = username;
		this.password = password;
		this.adresa = adresa;
		this.telefon = telefon;
		this.email = email;
		this.uloge_uloga_id = uloge_uloga_id;
	}





	public int getId_korisnik() {
		return id_korisnik;
	}

	public void setId_korisnik(int id_korisnik) {
		this.id_korisnik = id_korisnik;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	

	




	public int getUloge_uloga_id() {
		return uloge_uloga_id;
	}





	public void setUloge_uloga_id(int uloge_uloga_id) {
		this.uloge_uloga_id = uloge_uloga_id;
	}





	@Override
	public String toString() {
		return "Korisnici [id_korisnik=" + id_korisnik + ", ime=" + ime + ", prezime=" + prezime + ", username="
				+ username + ", password=" + password + ", adresa=" + adresa + ", telefon=" + telefon + ", email="
				+ email + ", uloga=" + uloge_uloga_id + "]";
	}
	
	
}
