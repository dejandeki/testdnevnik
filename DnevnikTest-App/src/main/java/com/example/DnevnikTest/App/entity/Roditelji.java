package com.example.DnevnikTest.App.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="roditelji")
public class Roditelji {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_roditelj")
	private int id_roditelj;
	
	//@OneToOne
	//@JoinColumn(name="korisnik_id_korisnik")
	//private Korisnici korisnik;
	
	private int korisnik_id_korisnik;
	public Roditelji() {}
	
	

	public Roditelji(int korisnik_id_korisnik) {
		super();
		this.korisnik_id_korisnik = korisnik_id_korisnik;
	}



	public int getKorisnik_id_korisnik() {
		return korisnik_id_korisnik;
	}



	public void setKorisnik_id_korisnik(int korisnik_id_korisnik) {
		this.korisnik_id_korisnik = korisnik_id_korisnik;
	}



	public int getId_roditelj() {
		return id_roditelj;
	}

	public void setId_roditelj(int id_roditelj) {
		this.id_roditelj = id_roditelj;
	}







	
	
	
	
}
