package com.example.DnevnikTest.App.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="uloge")
public class Uloge {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_uloga")
	private int id_uloga;
	
	@Column(name="uloga")
	private String uloga;
	
	
	
	
	
	public Uloge() {}

	public Uloge(String uloga) {
		
		this.uloga = uloga;
	}

	public int getId_uloga() {
		return id_uloga;
	}

	public void setId_uloga(int id_uloga) {
		this.id_uloga = id_uloga;
	}

	public String getUloga() {
		return uloga;
	}

	public void setUloga(String uloga) {
		this.uloga = uloga;
	}

	
	
	
	
}
