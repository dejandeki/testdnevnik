package com.example.DnevnikTest.App;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.DnevnikTest.App.entity.Uloge;
import com.example.DnevnikTest.App.service.UlogeService;





	@Controller
	//@RequestMapping("/korisnici")
	public class IndexController {

		@Autowired
		private UlogeService ulogeService;
		
		@RequestMapping("/list")
		public String listUloge(Model theModel) {

			// get cats 
			List<Uloge> theUloge = ulogeService.getAll();

			// add the cats to the model
			theModel.addAttribute("uloge", theUloge);

			return "listuloge";
		}
		
		@RequestMapping("/")
		public String index() {
			return "index";
		}
		
}
