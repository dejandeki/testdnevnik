package com.example.DnevnikTest.App.dao;

import com.example.DnevnikTest.App.entity.Roditelji;

public interface RoditeljiDAO extends GenericDAO<Roditelji> {

}
