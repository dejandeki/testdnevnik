package com.example.DnevnikTest.App.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="predmeti")
public class Predmeti {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_predmet")
	private int id_predmet;
	
	@Column(name="naziv")
	private String naziv;

	
	public Predmeti() {}


	public Predmeti( String naziv) {
		
		this.naziv = naziv;
	}


	public int getId_predmet() {
		return id_predmet;
	}


	public void setId_predmet(int id_predmet) {
		this.id_predmet = id_predmet;
	}


	public String getNaziv() {
		return naziv;
	}


	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	
	
	
}
