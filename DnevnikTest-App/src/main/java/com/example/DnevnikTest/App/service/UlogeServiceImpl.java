package com.example.DnevnikTest.App.service;

import org.springframework.stereotype.Service;

import com.example.DnevnikTest.App.dao.UlogeDAO;
import com.example.DnevnikTest.App.entity.Uloge;
@Service("ulogeService")
public class UlogeServiceImpl extends GenericServiceImpl<Uloge, UlogeDAO> implements UlogeService {

	
}
