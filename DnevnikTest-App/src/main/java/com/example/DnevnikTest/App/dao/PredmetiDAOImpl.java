package com.example.DnevnikTest.App.dao;

import org.springframework.stereotype.Repository;

import com.example.DnevnikTest.App.entity.Predmeti;

@Repository("predmetiDAO")
public class PredmetiDAOImpl extends GenericDAOImpl<Predmeti> implements PredmetiDAO {

	public PredmetiDAOImpl() {
		super(Predmeti.class);
		// TODO Auto-generated constructor stub
	}

}
