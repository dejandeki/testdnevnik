package com.example.DnevnikTest.App.dao;

import com.example.DnevnikTest.App.entity.Predmeti;

public interface PredmetiDAO extends GenericDAO<Predmeti> {

}
