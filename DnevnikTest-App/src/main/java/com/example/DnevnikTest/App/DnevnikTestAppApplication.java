package com.example.DnevnikTest.App;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DnevnikTestAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(DnevnikTestAppApplication.class, args);
	}

}
