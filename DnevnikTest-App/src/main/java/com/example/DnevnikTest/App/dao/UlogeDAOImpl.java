package com.example.DnevnikTest.App.dao;

import org.springframework.stereotype.Repository;

import com.example.DnevnikTest.App.entity.Uloge;

@Repository("ulogeDAO")
public class UlogeDAOImpl extends GenericDAOImpl<Uloge> implements UlogeDAO {

	public UlogeDAOImpl() {
		super(Uloge.class);
		// TODO Auto-generated constructor stub
	}

}
