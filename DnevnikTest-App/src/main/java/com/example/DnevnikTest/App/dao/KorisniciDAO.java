package com.example.DnevnikTest.App.dao;

import com.example.DnevnikTest.App.entity.Korisnici;

public interface KorisniciDAO extends GenericDAO<Korisnici> {

}
